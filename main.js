let containerElementId = document.getElementById("container");
// console.log(containerElementId);
// console.log(containerElementId.textContent);

let containerElementQuery = document.querySelector("#container");
// console.log(containerElementQuery);
// console.log(containerElementQuery.textContent);

let classSecondElements = document.querySelectorAll("li.second")
// console.log(classSecondElements);


let classThirdOlELement = document.querySelector("ol li.third");
// console.log(classThirdOlELement);


// let newPara = document.createElement("p");
// newPara.textContent = "Hello"
// containerElementId.prepend(newPara);
containerElementId.append("Hello!");

let footerContainer = document.querySelector("div.footer");
// console.log(footerContainer);
footerContainer.classList.add("main");
footerContainer.classList.remove("main");

let newLiElement = document.createElement("li");
newLiElement.textContent = "four";

containerElementId.querySelector("ul").appendChild(newLiElement);

// let olListElements = document.getElementsByTagName("ol");

// for(let list = 0; list < olListElements.length; list++) {
//     olListElements.item(list).style.backgroundColor = "green";
// }

let olListElements = document.querySelectorAll("ol li")

for(let list of olListElements) {
    list.style.backgroundColor = "green";
}
//item() returns element at specified index

// footerContainer.parentNode.removeChild(footerContainer);
// document.body.removeChild(footerContainer);
// footerContainer.outerHTML = "";
footerContainer.remove();